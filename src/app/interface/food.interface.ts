export interface FoodI {
  id: number;
  name: string;
}

export interface DishI extends FoodI {
  foodId: number;
}

export interface SpecificFoodI extends FoodI {
  specificId: number;
}
